import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { DataSource } from '@angular/cdk/table';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  httpSubscription: Subscription;
  url = 'https://habr.com';
  dataSource = [];
  isLoading = false;
  maxSaving = 0;

  displayedColumns: string[] = ['title', 'savings'];

  constructor(private http: HttpClient) {}

  ngOnInit() {
    
  }

  ngOnDestroy() {
    if (this.httpSubscription){
      this.httpSubscription.unsubscribe();
    }
  }

  onSubmit(form) {
    this.loadAudit(form.value.url);
  }

  loadAudit(url) {
    this.isLoading = true;
    this.httpSubscription = this.http.get(`http://localhost:3000/api/audit/?url=${url}`).subscribe((data)=>{
      this.dataSource = [];
      for (const key in data) {
        if (data[key].details && data[key].details.overallSavingsMs && data[key].details.overallSavingsMs > 0) {
          this.dataSource.push({
            title: data[key].title,
            savings: data[key].details.overallSavingsMs,
            description: data[key].description
          });
        }
      }
      this.dataSource.sort((a,b) => {
        return b.savings - a.savings;
      });
      this.maxSaving = this.dataSource[0].savings;
      this.isLoading = false;
    });
  }
}
