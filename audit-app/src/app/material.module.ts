import { NgModule } from '@angular/core';
import { MatTableModule, MatProgressSpinnerModule, MatButtonModule, MatInputModule, MatProgressBarModule} from '@angular/material';

@NgModule({
  imports: [
    MatTableModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatInputModule,
    MatProgressBarModule
  ],
  exports: [
    MatTableModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatInputModule,
    MatProgressBarModule
  ]
})
export class MaterialModule { }
